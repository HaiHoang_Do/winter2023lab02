import java.util.Scanner;

public class PartThree
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Please enter the first number:");
		double a = scan.nextDouble();
		System.out.println("Please enter the Second number:");
		double b = scan.nextDouble();

		System.out.println("Addition: " + Calculator.addition(a, b));
		System.out.println("Subtraction: " + Calculator.subtraction(a, b));
		
		Calculator cl = new Calculator();
		System.out.println("Multiplication: " + cl.multiplication(a, b));
		System.out.println("Division: " + cl.division(a, b));
	}
}