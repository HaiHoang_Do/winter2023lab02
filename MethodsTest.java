public class MethodsTest
{
    public static void main(String[] args)
    {
        int x = 5;
        System.out.println(x);
        methodNoInputNoReturn();
        System.out.println(x);

		System.out.println(x);
        methodOneInputNoReturn(x+10);
		System.out.println(x);
		
		methodTWoInputNoReturn(1, 2.5);
		
		int z = methodNoInputReturnInt();
		System.out.println(z);
		
		double sqrt = sumSquareRoot(9, 5);
		System.out.println(sqrt);
		
		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());

		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
		
    }

    public static void methodNoInputNoReturn()
    {
        System.out.println("I'm in a method that takes no input and returns nothing");
        int x = 20;
        System.out.println(x);
    }

    public static void methodOneInputNoReturn(int input)
    {
        System.out.println("Inside the method one input no return");
        input = input - 5;
        System.out.println(input);
        
    }
	
	public static void methodTWoInputNoReturn(int integer, double db)
	{
		System.out.println(integer);
		System.out.println(db);
	}
	
	public static int methodNoInputReturnInt()
	{
		int x = 5;
		return x;
	}
	
	public static double sumSquareRoot(int a, int b)
	{
		double z = Math.sqrt(a+b);
		return z;
	}
}